package com.epam;

import java.util.Scanner;
/*
* @author Taras Katsora
*
 */
public class TaskFirst {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введіть перше число");
        int firstNum = in.nextInt();
        System.out.println("Введіть останнє число");
        int lastNum = in.nextInt();
        int summ=0;
        System.out.println("Summ of Odd numbers " + getOddNumbers(firstNum, lastNum, summ));
        System.out.println("Summ of Even numbers " + getEvenNumbersReverted(firstNum, lastNum,summ));
    }

    public static int getOddNumbers(int firstNum, int lastNum, int summ) {
        /*
        * method that print summ of
        * odd numbers and odd numbers
        * from begin to the end
         */
        for (int i = firstNum; i <= lastNum; i++) {
            if (i % 2 != 0) {
                summ+=i;
                System.out.println(i);
            }
        }
        return summ;
    }

    public static int getEvenNumbersReverted(int firstNum, int lastNum, int summ) {
        /*
         * method that print summ of
         * Even numbers and Even numbers
         * from begin to the end
         */
        for (int i = lastNum; i >= firstNum; i--) {
            if (i % 2 == 0) {
                summ+=i;
                System.out.println(i);
            }
        }
        return summ;
    }
}