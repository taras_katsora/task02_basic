package com.epam;

import java.util.Scanner;
/*
 * @author Taras Katsora
 *
 */
public class ThirdTask {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введіть величину масиву");
        int size = in.nextInt();
        System.out.println(getFibonachiNumbers(size));
    }

    public static int getFibonachiNumbers(int size) {
        /*
        *method that build Fibonache numbers
        * count summ of odd and even numbers
        * print all odd and even numbers
        * count odd and even numbers perstage
         */
        int f = 0;
        int f1 = 1;
        int f2 = 0;
        float sumEven = 0;
        float sumOdd = 0;
        float perstageOdd = 0;
        float perstageEven = 0;
        int maxFibonacheEven = f2;
        int maxFibonacheOdd = f2;
        for (int i = 1; i < size; i++) {
            f = f1;
            f1 = f2;
            f2 = f + f1;
            System.out.println(f2);
            if (f2 % 2 == 0) {

                if (maxFibonacheEven < f2) {
                    sumEven++;
                    maxFibonacheEven = f2;
                }
            }
            if (f1 % 2 != 0) {
                if (maxFibonacheOdd < f1) {
                    sumOdd++;
                    maxFibonacheOdd = f1;
                }
            }
        }
        perstageEven = f2 / sumEven;
        perstageOdd = f2 / sumOdd;
        System.out.println("Perstage Even numbers= " + perstageEven + " %");
        System.out.println("Perstage Odd numbers= " + perstageOdd + " %");
        System.out.println("Max fibonacheEven = " + maxFibonacheEven);
        System.out.println("Max fibonacheOdd = " + maxFibonacheOdd);
        return f2;
    }
}